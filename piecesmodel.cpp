#include "piecesmodel.h"
#include "pieces/abstractpiece.h"
#include <QPoint>
#include "fenparser.h"
#include <QDebug>

PiecesModel::PiecesModel()
    :m_isEmpty(true)
{
}

void PiecesModel::parse(QString s) {

    Game::Side activeSide;
    int moveNumber;
    auto fenTuple = FenParser().read(s);
    std::tie(m_pieces, activeSide, moveNumber) = fenTuple;
    m_isEmpty = false;
    updateModel();
}

int PiecesModel::rowCount(const QModelIndex &/*parent = QModelIndex()*/) const {
    int r = m_isEmpty ? 0 : m_pieces.size() * BOARD_SIZE;
    return r;
}

QVariant PiecesModel::data(const QModelIndex &, int ) const {

    return QVariant();
}

char PiecesModel::getLetter(int index) {

    PiecePtr p = nullptr;
    if (index >= 0 && index < BOARD_SIZE * BOARD_SIZE) {
        p = at(index);
    }
    return p ? p->letter() : ' ';
}

void PiecesModel::clear() {

    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            at(QPoint(i, j)).reset();
        }
    }
    m_isEmpty = true;
    updateModel();
}

const PiecePtr& PiecesModel::at(const QPoint &p) const {
    return m_pieces.at(p.y()).at(p.x());
}

PiecePtr& PiecesModel::at(const QPoint &p) {
    return m_pieces.at(p.y()).at(p.x());
}

void PiecesModel::updateModel() {

    beginInsertRows(QModelIndex(), 0, 63);
    endInsertRows();
}

const PiecePtr& PiecesModel::at(int index) const {

    int x = index % 8;
    int y = index / 8;
    return m_pieces.at(y).at(x);
}

