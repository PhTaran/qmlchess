#include <QApplication>
#include <QQuickView>
#include <QWidget>
#include <QQmlContext>
#include <qqml.h>
#include <QQmlApplicationEngine>
#include <QGenericMatrix>


#include "gamecontroller.h"
#include "board.h"
#include "pieces/abstractpiece.h"

int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    qmlRegisterType<PiecesModel>("Pieces", 1, 0, "PiecesModel");
    QQuickView view;
    GameController *game = new GameController;
    view.rootContext()->setContextProperty("game", game);
    view.rootContext()->setContextProperty("myModel", game->model());
    view.setSource(QUrl(QStringLiteral("qrc:/qml/MainWindow.qml")));

    auto container = QWidget::createWindowContainer(&view);
    container->setMinimumSize(640, 480);
    container->setFocusPolicy(Qt::TabFocus);
    container->setWindowTitle(QObject::tr("Chess"));
    container->show();

    return app.exec();
}
