#pragma once

#include "abstractpiece.h"

class Bishop : virtual public AbstractPiece
{
public:
    Bishop(Game::Side t_side);

    AbstractPiece::Type type() const override { return AbstractPiece::Type::Bishop; }

protected:
    bool areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                             const Board& t_board) const override;
};

