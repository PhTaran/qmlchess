#include "king.h"
#include "board.h"

King::King(Game::Side t_side)
    : AbstractPiece(t_side)
{
}

bool King::areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                               const Board& t_board) const
{
    Q_UNUSED(t_board)

    auto xDelta = t_to.x() - t_from.x();
    auto yDelta = t_to.y() - t_from.y();

    if (xDelta >= -1 && xDelta <= 1 && yDelta >= -1 && yDelta <= 1)
        return true;
    return false;
}

