#pragma once

#include "abstractpiece.h"

class Knight : public AbstractPiece
{
public:
    Knight(Game::Side t_side);

    AbstractPiece::Type type() const override { return AbstractPiece::Type::Knight; }

private:
    bool areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                             const Board& t_board) const override;

    bool check(const QPoint& t_from, const QPoint& t_to, int t_left, int t_right) const;
};
