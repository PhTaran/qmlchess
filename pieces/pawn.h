#pragma once

#include "abstractpiece.h"

class Pawn : public AbstractPiece
{
public:
    explicit Pawn(Game::Side t_side);

    AbstractPiece::Type type() const override { return AbstractPiece::Type::Pawn; }

private:
    bool areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                             const Board& t_board) const override;

    bool check(const QPoint& t_from, const QPoint& t_to) const;

};

