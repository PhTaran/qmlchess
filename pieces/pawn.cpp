#include "pawn.h"
#include "board.h"

Pawn::Pawn(Game::Side t_side)
    : AbstractPiece(t_side)
{
}

bool Pawn::areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                               const Board& t_board) const
{
    const auto& dest = t_board.at(t_to);
    if (!dest) {
        // Destination square is unoccupied
        if (t_from.x() == t_to.x())
            if (check(t_from, t_to))
                return true;
    } else {
        // Destination hold piece of opposite side
        if((t_from.x() == t_to.x() + 1) || (t_from.x() == t_to.x() - 1))
            if (check(t_from, t_to))
                return true;
    }
    return false;
}

bool Pawn::check(const QPoint& t_from, const QPoint& t_to) const
{
    if (side() == Game::Side::Black) {
        if (t_from.y() == 1 && t_to.y() == t_from.y() + 2)
            return true;
        if (t_to.y() == t_from.y() + 1)
            return true;
    } else {
        if (t_from.y() == 6 && t_to.y() == t_from.y() - 2)
            return true;
        if (t_to.y() == t_from.y() - 1)
            return true;
    }
    return false;
}

