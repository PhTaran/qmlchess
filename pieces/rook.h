#pragma once

#include "abstractpiece.h"

class Rook : virtual public AbstractPiece
{
public:
    Rook(Game::Side t_side);

    AbstractPiece::Type type() const override { return AbstractPiece::Type::Rook; }

protected:
    bool areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                             const Board& t_board) const override;

    bool check(const QPoint& t_from, const QPoint& t_to,
               const Board& t_board, bool t_x) const;
};


