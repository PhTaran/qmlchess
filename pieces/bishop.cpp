#include "bishop.h"
#include "board.h"

Bishop::Bishop(Game::Side t_side)
    : AbstractPiece(t_side)
{
}

bool Bishop::areSquaresPermitted(const QPoint &t_from, const QPoint &t_to,
                                 const Board& t_board) const
{
    if ((t_to.y() - t_from.y() == t_to.x() - t_from.x())
        || (t_to.y() - t_from.y() == t_from.x() - t_to.x())) {
        // Make sure that all invervening squares are empty
        auto xOffset = (t_to.x() - t_from.x() > 0) ? 1 : -1;
        auto yOffset = (t_to.y() - t_from.y() > 0) ? 1 : -1;

        auto checkx = 0, checky = 0;
        for (checkx = t_from.x() + xOffset, checky = t_from.y() + yOffset;
             checkx != t_to.x();
             checkx += xOffset, checky += yOffset)
        {
            if (t_board.at({checkx, checky}))
                return false;
        }
        return true;
    }
    return false;
}

