#include "knight.h"
#include "board.h"

Knight::Knight(Game::Side t_side)
    : AbstractPiece(t_side)
{
}

bool Knight::areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                                 const Board& t_board) const
{
    Q_UNUSED(t_board)
    // Destination square is unoccupied or occupied by opposite color
    if (check(t_from, t_to, 1, 2) || check(t_from, t_to, 2, 1))
        return true;
    return false;
}

bool Knight::check(const QPoint& t_from, const QPoint& t_to,
                   int t_left, int t_right) const
{
    if (((t_from.y() == t_to.y() + t_left)
         || (t_from.y() == t_to.y() - t_left))
            && ((t_from.x() == t_to.x() + t_right)
                || (t_from.x() == t_to.x() - t_right)))
        return true;
    return false;
}

