#include "rook.h"
#include "board.h"

Rook::Rook(Game::Side t_side)
    : AbstractPiece(t_side)
{
}

bool Rook::areSquaresPermitted(const QPoint& t_from, const QPoint& t_to,
                               const Board& t_board) const
{
    if (t_from.x() == t_to.x())
        return check(t_from, t_to, t_board, true);
    else if (t_from.y() == t_to.y())
        return check(t_from, t_to, t_board, false);

    return false;
}

bool Rook::check(const QPoint& t_from, const QPoint& t_to,
                 const Board& t_board, bool t_x) const
{
    auto from = t_x ? t_from.y() : t_from.x();
    auto to = t_x ? t_to.y() : t_to.x();

    // Make sure that all invervening squares are empty
    auto offset = (to - from > 0) ? 1 : -1;
    for (auto checkVal = from + offset; checkVal != to; checkVal += offset) {
        auto left = t_x ? t_from.x() : checkVal;
        auto right = t_x ? checkVal : t_from.y();
        if (t_board.at({ left, right }))
            return false;
    }
    return true;
}

