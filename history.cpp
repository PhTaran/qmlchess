#include "history.h"

void UndoCommand::redo() {
    m_piece = m_pBoard.at(m_dst);
    m_pBoard.move(m_src, m_dst);
}

void UndoCommand::undo() {
//        m_pBoard->move(m_dst, m_src);
    m_pBoard.at(m_src) = m_pBoard.at(m_dst);
    m_pBoard.at(m_dst) = m_piece;
    m_pBoard.model()->updateModel();
}

QDataStream& operator <<(QDataStream &dt, const UndoCommand &tr) {
    dt << tr.src() << tr.dst();
    return dt;
}

QDataStream& operator >>(QDataStream &dt, UndoCommand &tr) {
    QPoint src, dst;
    dt >> src;
    dt >> dst;
    tr.setSrc(src);
    tr.setDst(dst);
    return dt;
}
