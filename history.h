#ifndef HISTORY_H
#define HISTORY_H

#include <QString>
#include <QPoint>
#include <QStack>
#include <QDataStream>
#include <QUndoCommand>
#include "board.h"
#include "pieces/abstractpiece.h"

class UndoCommand : public QUndoCommand {

public:
    UndoCommand(Board &pBoard, QPoint src, QPoint dst) :m_pBoard(pBoard), m_src(src), m_dst(dst) {}

    void redo();
    void undo();

    void setSrc(const QPoint &p) { m_src = p; }
    void setDst(const QPoint &p) { m_dst = p; }

    QPoint src() const { return m_src; }
    QPoint dst() const { return m_dst; }

private:
    Board &m_pBoard;
    QPoint m_src;
    QPoint m_dst;
    PiecePtr m_piece;
};

QDataStream& operator <<(QDataStream &dt, const UndoCommand &tr);
QDataStream& operator >>(QDataStream &dt, UndoCommand &tr);

#endif // HISTORY_H
