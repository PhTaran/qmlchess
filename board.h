#pragma once

#include <array>
#include <memory>
#include <stdexcept>
#include "game.h"
#include <QPoint>
#include <QVector>
#include "piecesmodel.h"
#include "definitions.h"
#include "history.h"

class AbstractPiece;

class Board: public QObject
{
    Q_OBJECT
public:

    static const QString START_FEN;

    Board(QObject *parent = 0);

    const PiecePtr& at(int x, int y) const
    {
        return at(QPoint(x, y));
    }
    PiecePtr& at(int x, int y)
    {
        return at(QPoint(x, y));
    }
    const PiecePtr& at(QPoint p) const
    {
        return m_model.at(p);
    }
    PiecePtr& at(QPoint p)
    {
        return m_model.at(p);
    }

    void setFen(const QString& t_fenStr);
    void setActiveSide(Game::Side t_side) { m_activeSide = t_side; }
    Game::Side activeSide() const { return m_activeSide; }

    void move(const QPoint& t_from, const QPoint& t_to);
    bool canMove(const QPoint& t_from, const QPoint& t_to) const;
    PiecesModel* model() { return &m_model; }

    void clear() { model()->clear(); }

signals:
    void changeModel();

private:
    Game::Side m_activeSide;
    int m_moveNumber;
    PiecesModel m_model;

    void placePieces(Game::Side t_side);
};

