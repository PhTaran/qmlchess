import QtQuick 2.4

import "Constants.js" as Constants

import Pieces 1.0

Item {
    id: root

    property int cellSize: width / Constants.BOARD_SIZE
    property int moveFromIndex: -1
    property int moveToIndex: -1

    height: parent.width
    width: height

    function isWhiteSide(index) {
        var letter = getLetter(index);
        return letter === letter.toUpperCase();
    }

    function getChar(ch) {
        var letter = ch
        var res = letter;
        if (letter === 'b') {
            res = 'l';
        } else if (letter === 'B') {
            res = 'L';
        }
        return res;
    }

    FontLoader {
        id: chessFont;
        source: "qrc:/fonts/" + Constants.CHESS_FONT
    }

    Grid {
        id: cellGrid

        property int cellSize: parent.cellSize
        property Piece selectedPiece: null

        anchors.fill: parent
        rows: Constants.BOARD_SIZE
        columns: Constants.BOARD_SIZE

        Repeater {

            id: repeater

            model: Constants.BOARD_SIZE * Constants.BOARD_SIZE

            DropArea {
                id: dropArea

                width: cellGrid.cellSize
                height: width

                property int cellX: index % 8
                property int cellY: index / 8

                Rectangle {
                    id: cell

                    anchors.fill: parent
                    border.width: Constants.BOARD_BORDER_WIDTH
                    border.color: Constants.BOARD_BORDER_COLOR

                    color: (Math.floor(index / Constants.BOARD_SIZE)
                        + (index % Constants.BOARD_SIZE)) % 2
                            ? Constants.CELL_DARK_COLOR : Constants.CELL_LIGHT_COLOR

                    states: [
                        State {
                            when: dropArea.containsDrag
                            PropertyChanges {
                                target: cell
                                color: "grey"
                            }
                        }
                    ]

                }
            }

        }

    }

    GridView {
        id: piecesView
        anchors.fill: parent

        cellWidth: cellGrid.cellSize
        cellHeight: cellGrid.cellSize

        interactive: false
        model: myModel

        delegate: Item {
            id: delegate
            Piece {
                id: piece

                cellSize: cellGrid.cellSize
                whiteSide: true
                letter: getChar(myModel.getLetter(index))
                pieceIndex: index
            }
        }
        Component.onCompleted: {
            myModel.updateModel()
        }
    }
}

