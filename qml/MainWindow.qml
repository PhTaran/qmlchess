import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.0

import "Constants.js" as Constants

Rectangle {
    id: main
    anchors.fill: parent

    color: Constants.BACKGOUND_COLOR;

    state: "screen1"

    RowLayout {
        id: root
        anchors.fill: parent
        spacing: 0

        Board {
            id: board
            objectName: "board"
            anchors.left: parent.left; anchors.top: parent.top; anchors.bottom: parent.bottom;
            anchors.margins: Constants.INDENT_DEFAULT
            width: height
        }

        ColumnLayout {

            anchors.left: board.right; anchors.right: parent.right;
            anchors.top: parent.top; anchors.bottom: parent.bottom
            anchors.margins: Constants.INDENT_DEFAULT
            spacing: Constants.INDENT_DEFAULT

            GameButton {
                id: btnStart
                text: "Start"
                onClicked: {

                    if (main.state == "screen1" || main.state == "screen3") {
                        main.state = "screen2"
                        game.loadPieces()
                    }
                    else if (main.state == "screen2") {
                        main.state = "screen1"
                        myModel.clear()
                    }
                }
            }

            GameButton {
                id: btnLoad
                text: "Load"
                onClicked: {
                    if (main.state == "screen1" || main.state == "screen3") {
                        fileDialog.open()
                    }
                    else if (main.state == "screen2") {
                        game.saveGame()
                    }
                }

                FileDialog {
                    id: fileDialog
                    title: "Please choose a file"
                    folder: shortcuts.home
                    nameFilters: [ "Chess files (*.chess)" ]
                    onAccepted: {
                        var status = game.loadSavedGame(fileDialog.fileUrl)
                        if (!status) {
                            messageDialog.title = "error!"
                            messageDialog.text = game.lastError
                            messageDialog.open()
                        }
                        else {
                            main.state = "screen3"
                        }
                    }
                }

                MessageDialog {
                    id: messageDialog
                }
            }

            GameButton {
                id: btnNext
                text: "next"
                opacity: 0
                onClicked: {
                    game.next()
                }
            }

            GameButton {
                id: btnPrev
                text: "prev"
                opacity: 0
                onClicked: {
                    game.previos()
                }
            }
        }
    }

    states: [
        State {
            name: "screen1"

            PropertyChanges {
                target: btnStart
                text: "start"
            }

            PropertyChanges {
                target: btnLoad
                text: "load"
            }
        },
        State {
            name: "screen2"

            PropertyChanges {
                target: btnStart
                text: "stop"
            }

            PropertyChanges {
                target: btnLoad
                text: qsTr("save")
            }
        },
        State {
            name: "screen3"
            PropertyChanges {
                target: btnStart
                text: qsTr("start")
            }

            PropertyChanges {
                target: btnLoad
                text: qsTr("load")
            }

            PropertyChanges {
                target: btnNext
                opacity: 1
            }
            PropertyChanges {
                target: btnPrev
                opacity: 1
            }
        }
    ]

}

