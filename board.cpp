#include <QTextStream>
#include <QDebug>

#include "pieces/abstractpiece.h"
#include "board.h"
#include "fenparser.h"

const QString Board::START_FEN =
    "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";


Board::Board(QObject *parent):
    QObject(parent),
    m_activeSide(Game::Side::White)
{
    QObject::connect(this, Board::changeModel, model(), PiecesModel::updateModel, Qt::QueuedConnection);
}

void Board::setFen(const QString& t_fenStr)
{
    auto fenTuple = FenParser().read(t_fenStr);
    PiecesMatrix mtx;
    std::tie(mtx, m_activeSide, m_moveNumber) = fenTuple;
    m_model.parse(t_fenStr);
}

void Board::move(const QPoint& t_from, const QPoint& t_to)
{
    auto currPiece = at(t_from);
    if (currPiece) {
        if (currPiece->isMovePermitted(t_from, t_to, *this)) {
            if (at(t_to)) {
                at(t_to).reset();
            }
            at(t_to) = currPiece;
            at(t_from).reset();
        }
    }
    m_moveNumber++;
    m_activeSide = m_moveNumber % 2 == 0 ? Game::Side::Black : Game::Side::White;
    emit changeModel();
}

bool Board::canMove(const QPoint &t_from, const QPoint &t_to) const {
    auto currPiece = at(t_from);

    if (currPiece) {
        qDebug() << "currPiece " << (*currPiece);
        return currPiece->side() == m_activeSide && currPiece->isMovePermitted(t_from, t_to, *this);
    }
    return false;
}
