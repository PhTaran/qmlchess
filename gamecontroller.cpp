#include "gamecontroller.h"

#include <QDebug>
#include <QFile>
#include <QUrl>
#include <QFileDialog>

GameController::GameController(QObject* t_parent)
    : QObject(t_parent)
{
}

bool GameController::canMove(const QPoint& from, const QPoint& to) {

    return m_board.canMove(from, to);
}

void GameController::move(const QPoint& from, const QPoint& to) {

    UndoCommand *undo = new UndoCommand(m_board, from, to);

    m_stack.push(undo);
}

void GameController::loadPieces() {

    m_board.setFen(m_board.START_FEN);
    m_stack.clear();
}

void GameController::next() {

    if (m_stack.canRedo()) {
        m_stack.redo();
    }
}

void GameController::previos() {

    if (m_stack.canUndo()) {
        m_stack.undo();
    }
}

bool GameController::loadSavedGame(const QUrl &url) {

    QFile f(url.toLocalFile());
    if (!f.open(QIODevice::ReadOnly)) {
        m_lastError = "File has not opened";
        return false;
    }

    try {

        QByteArray s = f.readAll();
        QDataStream dt(s);
        loadPieces();
        m_stack.clear();
        int countCommands = 0;

        dt >> countCommands;

        for (int i = 0; i < countCommands; ++i) {

            UndoCommand *p = new UndoCommand(m_board, QPoint(), QPoint());
            dt >> (*p);
            m_stack.push(p);
        }
    } catch (...) {
        m_lastError = "error on processing saved game. May file is corrupted or file format is not correct";
        m_board.clear();
        return false;
    }
    return true;
}

void GameController::saveGame(/*const QUrl &url*/) {

    QString fileName = QFileDialog::getSaveFileName(0, tr("Save File"), "./", tr("Chess files (*.chess)"));

    QFile f(fileName);
    f.open(QIODevice::WriteOnly);

    QByteArray buff;
    QDataStream dt(&buff, QIODevice::WriteOnly);

    dt << m_stack.count();

    for (int i = 0; i < m_stack.count(); ++i) {
        const UndoCommand *cmd = dynamic_cast<const UndoCommand*>(m_stack.command(i));
        dt << (*cmd);
    }

    f.write(buff);
    f.close();
}
