#ifndef PIECESMODEL_H
#define PIECESMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <array>
#include "pieces/abstractpiece.h"
#include "definitions.h"

class PiecesModel : public QAbstractListModel
{
    Q_OBJECT
public:

    PiecesModel();

    void parse(QString s);

    Q_INVOKABLE virtual int rowCount(const QModelIndex & = QModelIndex()) const;
    Q_INVOKABLE virtual QVariant data(const QModelIndex &, int = Qt::DisplayRole) const;

    Q_INVOKABLE char getLetter(int index);
    Q_INVOKABLE void clear();
    const PiecePtr& at(const QPoint &p) const;
    PiecePtr& at(const QPoint &p);

public slots:
    void updateModel();

private:

    const PiecePtr& at(int index) const;

    PiecesMatrix m_pieces;
    bool m_isEmpty;
};

#endif // PIECESMODEL_H
