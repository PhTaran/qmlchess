#pragma once

#include <QObject>
#include <QPoint>

#include "board.h"
#include "piecesmodel.h"

#include <QUndoStack>

class GameController : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString lastError READ lastError)

public:
    explicit GameController(QObject* t_parent = nullptr);

    Q_INVOKABLE bool canMove(const QPoint& from, const QPoint& to);
    Q_INVOKABLE void move(const QPoint& from, const QPoint& to);

    const Board& board() const { return m_board; }
    Board& board() { return m_board; }

    PiecesModel* model() { return m_board.model(); }

    Q_INVOKABLE void loadPieces();

    Q_INVOKABLE void next();
    Q_INVOKABLE void previos();

    Q_INVOKABLE bool loadSavedGame(const QUrl &url);
    Q_INVOKABLE void saveGame();

    QString lastError() { return m_lastError; }

signals:

public slots:

private:
    Board m_board;
    QUndoStack m_stack;
    QString m_lastError;
};

