#ifndef DEFINITIONS
#define DEFINITIONS

#include "pieces/abstractpiece.h"
#include <array>

static const int BOARD_SIZE = 8;

using PiecePtr = std::shared_ptr<AbstractPiece>;
using PiecesMatrix = std::array<std::array<PiecePtr, BOARD_SIZE>, BOARD_SIZE>;


#endif // DEFINITIONS

